#!/bin/sh
# Usage: sh autogen.sh [ADDITIONAL-AUTORECONF-OPTIONS...]

# Copyright (C) 2003, 2008, 2011-2013, 2020, 2022 Thien-Thi Nguyen
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

for p in automake autoconf libtoolize guile-baux-tool guile ; do
    $p --version | sed 's/^/using: /;1q'
done

guile-baux-tool snuggle m4 build-aux
guile-baux-tool import \
    re-prefixed-site-dirs \
    c2x \
    gen-scheme-wrapper \
    sofix \
    uninstall-sofixed \
    gbaux-do

set -x
exec autoreconf --verbose -i -s "$@"

# autogen.sh ends here
