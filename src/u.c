/* u.c --- module (upshot)

   Copyright (C) 2004, 2008, 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "config-from-configure.h"
#include "hey.h"
#include "gi.h"
#include "mim.h"
#include "oh-btw.h"

/* begin use decl */

IMPORT_MODULE (upstream_module, "(random)");
SELECT_MODULE_VAR (rr, upstream_module, "rr");

IMPORT_MODULE (another_upstream, "(tamped)");
SELECT_MODULE_VAR (ab, another_upstream, "access-btw");

/* end use decl */

static struct btw *btw;

THIS_MODULE (cur);

SCM_SYMBOL (u_sym_u, "u");

PRIMPROC (uu, "uu", 0, 0, 0, (void), "")
{
  return PCHAIN (u_sym_u,
                 cur,
                 rr,
                 CALL0 (rr));
}

#define answer  (*btw->the_answer)

PRIMPROC (u_inc_x, "u-inc!", 0, 0, 0, (SCM n), "")
{
  answer++;
  return NUM_ULONG (answer);
}

static
void
init_module (void)
{
  HEY_STARTING ();
#include "u.x"
  btw = UNPACK_POINTER (CALL0 (ab));
  HEY_BTW (btw, answer);
  HEY_FINISHED ();
}

MOD_INIT_LINK_THUNK ("upshot", upshot, init_module)

/* u.c ends here */
