/* t.c --- module (tamped)

   Copyright (C) 2004, 2008, 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "config-from-configure.h"
#include "hey.h"
#include "gi.h"
#include "mim.h"
#include "oh-btw.h"

/* begin use decl */

IMPORT_MODULE (upstream_module, "(random)");
SELECT_MODULE_VAR (rr, upstream_module, "rr");

/* end use decl */

static int answer = 42;

THIS_MODULE (cur);

SCM_SYMBOL (t_sym_t, "t");

PRIMPROC (tt, "tt", 0, 0, 0, (void), "")
{
  return PCHAIN (t_sym_t,
                 cur,
                 rr);
}

static struct btw btw =
  {
    .the_answer = &answer
  };

PRIMPROC (access_btw, "access-btw", 0, 0, 0, (void), "")
{
  return PACK_POINTER (&btw);
}

PRIMPROC (t_inc_x, "t-inc!", 0, 0, 0, (void), "")
{
  answer++;
  return NUM_ULONG (answer);
}

static
void
init_module (void)
{
  HEY_STARTING ();
  HEY_BTW (&btw, answer);
#include "t.x"
  HEY_FINISHED ();
}

MOD_INIT_LINK_THUNK ("tamped", tamped, init_module)

/* t.c ends here */
