/* gi.h --- guile interface

   Copyright (C) 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <libguile.h>
#ifdef HAVE_GUILE_MODSUP_H
#include <guile/gh.h>
#endif

#define PERMANENT   scm_permanent_object

#ifdef HAVE_GUILE_MODSUP_H

#define LISTIFY      gh_list
#define C_ULONG      gh_scm2ulong
#define NUM_ULONG    gh_ulong2scm
#define READ_STRING  scm_read_0str
#define CALL0        gh_call0

#define CURRENT_MODULE  scm_selected_module
#define MODULE_LOOKUP   gh_module_lookup

#else  /* !defined HAVE_GUILE_MODSUP_H */

#define LISTIFY      scm_list_n
#define C_ULONG      scm_to_ulong
#define NUM_ULONG    scm_from_ulong
#define READ_STRING  scm_c_read_string
#define CALL0        scm_call_0

#define CURRENT_MODULE  scm_current_module
#define MODULE_LOOKUP(module, name)                     \
  scm_variable_ref (scm_c_module_lookup (module, name))

#endif  /* !defined HAVE_GUILE_MODSUP_H */

#define PACK_POINTER(x)    (scm_object_address (PTR2SCM (x)))
#define UNPACK_POINTER(x)  ((void *) C_ULONG (x))

#define PCHAIN(...)  (LISTIFY (__VA_ARGS__, SCM_UNDEFINED))

/* gi.h ends here */
