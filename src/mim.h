/* mim.h --- module interface manglement

   Copyright (C) 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <libguile.h>

#define THIS_MODULE(cvar)                               \
SCM_SNARF_HERE (static SCM cvar)                        \
SCM_SNARF_INIT (cvar = PERMANENT (CURRENT_MODULE ());)

#ifdef HAVE_GUILE_MODSUP_H

#include <guile/gh.h>
#include <guile/modsup.h>
#define IMPORT_MODULE      GH_USE_MODULE
#define SELECT_MODULE_VAR  GH_SELECT_MODULE_VAR
#define PRIMPROC           GH_DEFPROC

#define MOD_INIT_LINK_THUNK  GH_MODULE_LINK_FUNC

#else  /* !defined HAVE_GUILE_MODSUP_H */

#define IMPORT_MODULE(cvar,fullname)                    \
SCM_SNARF_HERE (static char s_ ## cvar[] =              \
                fullname "#_#_"; static SCM cvar)       \
SCM_SNARF_INIT (cvar = PERMANENT                        \
                (scm_resolve_module                     \
                 (READ_STRING (s_ ## cvar)));)

#define SELECT_MODULE_VAR(cvar,m_cvar,s_name)                           \
SCM_SNARF_HERE (static SCM cvar)                                        \
SCM_SNARF_INIT (cvar = PERMANENT (MODULE_LOOKUP (m_cvar, s_name));)

#define PRIMPROC(FNAME, PRIMNAME, REQ, OPT, VAR, ARGLIST, DOCSTRING)    \
SCM_SNARF_HERE (                                                        \
static const char s_ ## FNAME [] = PRIMNAME;                            \
static SCM FNAME ARGLIST                                                \
)                                                                       \
SCM_SNARF_INIT (scm_c_define_gsubr                                      \
                (s_ ## FNAME, REQ, OPT, VAR,                            \
                 (SCM_FUNC_CAST_ARBITRARY_ARGS) FNAME);)                \
SCM_SNARF_DOCS (primitive, FNAME, PRIMNAME, ARGLIST,                    \
                REQ, OPT, VAR, DOCSTRING)

#define MOD_INIT_LINK_THUNK(pretty,frag,func)           \
void scm_init_ ## frag ## _module (void) { func (); }

#endif  /* !defined HAVE_GUILE_MODSUP_H */

/* mim.h ends here */
