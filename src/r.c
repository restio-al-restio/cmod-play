/* r.c --- module (random)

   Copyright (C) 2004, 2008, 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "config-from-configure.h"
#include "hey.h"
#include "gi.h"
#include "mim.h"

SCM_SYMBOL (r_sym_r, "r");

THIS_MODULE (cur);

PRIMPROC (rr, "rr", 0, 0, 0, (void), "")
{
  return PCHAIN (r_sym_r,
                 cur);
}

static
void
init_module (void)
{
  HEY_STARTING ();
#include "r.x"
  HEY_FINISHED ();
}

MOD_INIT_LINK_THUNK ("random", random, init_module)

/* r.c ends here */
