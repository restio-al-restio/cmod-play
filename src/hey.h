/* hey.h --- simple tracing

   Copyright (C) 2004, 2008, 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#define HEY_MESSAGE(m,f)  if (getenv ("VERBOSE")) fprintf (stderr, (m), (f))
#define HEY_ACTION(a)     HEY_MESSAGE (__FILE__ ": " a " %s()\n", __func__)
#define HEY_STARTING()    HEY_ACTION ("starting")
#define HEY_FINISHED()    HEY_ACTION ("finished")

/* hey.h ends here */
