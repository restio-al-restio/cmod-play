/* oh-btw.h --- oh by the way (a "C++ private class" for C)

   Copyright (C) 2013 Thien-Thi Nguyen

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

struct btw {
  int *the_answer;
};

#define HEY_BTW(btw,answer)  do                         \
    {                                                   \
      HEY_MESSAGE ("     btw: %p\n", (void *) btw);     \
      HEY_MESSAGE ("  answer: %d\n", answer);           \
    }                                                   \
  while (0)

/* oh-btw.h ends here */
